<?php

interface ClientPublicAPIInterface
{
    public function returnTicker();
    public function return24Volume();
    public function returnOrderBook();
    public function returnTradeHistory();
    public function returnChartData();
    public function returnCurrencies();
    public function returnLoanOrders();
}
