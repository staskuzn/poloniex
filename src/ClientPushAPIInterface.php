<?php

interface ClientPushAPIInterface
{
    public function getTicker();
    public function getOrderBookAndTrades();
    public function getTrollBoxMessages();
}
