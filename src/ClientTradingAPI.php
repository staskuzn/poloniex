<?php namespace stcom77\Poloniex;

use ClientTradingAPIInterface;

class ClientTradingAPI implements ClientTradingAPIInterface
{
    private $key;
    private $sign;

    public function __construct($key, $sign)
    {
        $this->key = $key;
        $this->sign = $sign;
    }

    public function returnBalances()
    {
        // TODO: Implement returnBalances() method.
    }

    public function returnCompleteBalances()
    {
        // TODO: Implement returnCompleteBalances() method.
    }

    public function returnDepositAddresses()
    {
        // TODO: Implement returnDepositAddresses() method.
    }

    public function generateNewAddress()
    {
        // TODO: Implement generateNewAddress() method.
    }

    public function returnOpenOrders()
    {
        // TODO: Implement returnOpenOrders() method.
    }

    public function returnTradeHistory()
    {
        // TODO: Implement returnTradeHistory() method.
    }

    public function returnOrderTrades()
    {
        // TODO: Implement returnOrderTrades() method.
    }

    public function buy()
    {
        // TODO: Implement buy() method.
    }

    public function sell()
    {
        // TODO: Implement sell() method.
    }

    public function cancelOrder()
    {
        // TODO: Implement cancelOrder() method.
    }

    public function moveOrder()
    {
        // TODO: Implement moveOrder() method.
    }

    public function withdraw()
    {
        // TODO: Implement withdraw() method.
    }

    public function returnFeeInfo()
    {
        // TODO: Implement returnFeeInfo() method.
    }

    public function returnAvailableAccountBalances()
    {
        // TODO: Implement returnAvailableAccountBalances() method.
    }

    public function returnTradableBalances()
    {
        // TODO: Implement returnTradableBalances() method.
    }

    public function transferBalance()
    {
        // TODO: Implement transferBalance() method.
    }

    public function returnMarginAccountSummary()
    {
        // TODO: Implement returnMarginAccountSummary() method.
    }

    public function marginBuy()
    {
        // TODO: Implement marginBuy() method.
    }

    public function marginSell()
    {
        // TODO: Implement marginSell() method.
    }

    public function getMarginPosition()
    {
        // TODO: Implement getMarginPosition() method.
    }

    public function closeMarginPosition()
    {
        // TODO: Implement closeMarginPosition() method.
    }

    public function createLoanOffer()
    {
        // TODO: Implement createLoanOffer() method.
    }

    public function cancelLoanOffer()
    {
        // TODO: Implement cancelLoanOffer() method.
    }

    public function returnOpenLoanOffers()
    {
        // TODO: Implement returnOpenLoanOffers() method.
    }

    public function returnActiveLoans()
    {
        // TODO: Implement returnActiveLoans() method.
    }

    public function returnLendingHistory()
    {
        // TODO: Implement returnLendingHistory() method.
    }

    public function toggleAutoRenew()
    {
        // TODO: Implement toggleAutoRenew() method.
    }

}
